HID_DIR = ./hidapi
HID_VER = 0.14.0
HID_URL = https://github.com/libusb/hidapi/archive/refs/tags/hidapi-$(HID_VER).tar.gz

CFLAGS = -O2 -I $(HID_DIR)/hidapi

ifeq "$(shell uname -s)" "Darwin"

CFLAGS += -I $(HID_DIR)/mac -arch x86_64 -arch arm64
LIBS = -framework IOKit -framework CoreFoundation -framework AppKit
HID_C = $(HID_DIR)/mac/hid.c

else

CFLAGS += $(shell pkg-config --cflags libudev)
LIBS = $(shell pkg-config --libs libudev)
HID_C = $(HID_DIR)/linux/hid.c

endif

.PHONY: clean cleanall

hidhornet: $(HID_C) hidhornet.c
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

$(HID_C):
	curl -LSsf -o hidapi.tar.gz $(HID_URL)
	tar xf hidapi.tar.gz
	rm -f hidapi.tar.gz
	mv hidapi-hidapi-$(HID_VER) $(HID_DIR)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f hidhornet

cleanall: clean
	rm -rf hidapi

