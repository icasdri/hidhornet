/*
 * Copyright 2023 icasdri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <signal.h>
#include <unistd.h>

#include "hidapi.h"

#ifdef __APPLE__
#include "hidapi_darwin.h"
#include <ApplicationServices/ApplicationServices.h>
#include <Carbon/Carbon.h>
#endif

#define APPNAME "hidhornet"

static int analog_trigger(const uint16_t, const uint16_t, const uint16_t, const uint16_t);
static void inject_input(const char, const bool);

/****************************************************************************
 * CONFIGURATION:
 *   Update the code/variabes here and re-compile.
 */

#define DEFAULT_DEVICE "default device not set";
#define DEFAULT_BUFLEN 64;
#define DEFAULT_TIMEOUT_MILLIS 1000

// Callback called for every new controller input
static void handle_input(uint8_t *data, int nread)
{
    // State tracking (note: static variables persist between function calls)
    static uint16_t old_left_trigger = 0;  // 0 to 1023
    static uint16_t old_right_trigger = 0;  // 0 to 1023
    static uint8_t old_select_button = 0;  // 0 or 1

    // Xbox One controller appears to emit actual data as 17-byte reports
    if (nread != 17)
        return;

    // Extract analog trigger values and select button state from raw data
    uint16_t new_left_trigger;
    memcpy(&new_left_trigger, &data[9], sizeof(new_left_trigger));

    uint16_t new_right_trigger;
    memcpy(&new_right_trigger, &data[11], sizeof(new_right_trigger));

    uint8_t new_select_button = data[16] & 0x1;

    //
    // Look for value changes and inject input as desired
    // Use 350 and 420 (out of 1023) as lo and hi thresholds for the triggers
    //
    int l = analog_trigger(old_left_trigger, new_left_trigger, 350, 420);
    if (l) {
        inject_input('s', (l > 0));
    }

    int r = analog_trigger(old_right_trigger, new_right_trigger, 350, 420);
    if (r) {
        inject_input('c', (r > 0));
    }

    if (new_select_button != old_select_button) {
        inject_input('i', (new_select_button > 0));
    }

    old_left_trigger = new_left_trigger;
    old_right_trigger = new_right_trigger;
    old_select_button = new_select_button;
}

/****************************************************************************/

static int exit_signal;  // set by signal handler
static uint8_t buf[1024];  // static buffer for raw data

typedef enum {
    MODE_LIST,
    MODE_WATCH,
    MODE_RUN
} my_mode_t;

/*
 * Helper function for handling analog trigger inputs.
 * Returns 0 = do nothing
 *         1 = triggered (new_val crosses into hi_thresh)
 *         -1 = de-triggered (new_val crosses into lo_thresh)
 */
static int analog_trigger(const uint16_t old_val, const uint16_t new_val,
                          const uint16_t lo_thresh, const uint16_t hi_thresh)
{
    if (new_val > old_val) {
        if ((old_val < hi_thresh) && (new_val >= hi_thresh)) {
            return 1;
        }
    } else if (new_val < old_val) {
        if ((old_val > lo_thresh) && (new_val <= lo_thresh)) {
            return -1;
        }
    }
    return 0;
}

#ifdef __APPLE__
static CGKeyCode darwin_get_keycode(const char key)
{
    switch (key) {
        case 'a': return kVK_ANSI_A;
        case 'b': return kVK_ANSI_B;
        case 'c': return kVK_ANSI_C;
        case 'd': return kVK_ANSI_D;
        case 'e': return kVK_ANSI_E;
        case 'f': return kVK_ANSI_F;
        case 'g': return kVK_ANSI_G;
        case 'h': return kVK_ANSI_H;
        case 'i': return kVK_ANSI_I;
        case 'j': return kVK_ANSI_J;
        case 'k': return kVK_ANSI_K;
        case 'l': return kVK_ANSI_L;
        case 'm': return kVK_ANSI_M;
        case 'n': return kVK_ANSI_N;
        case 'o': return kVK_ANSI_O;
        case 'p': return kVK_ANSI_P;
        case 'q': return kVK_ANSI_Q;
        case 'r': return kVK_ANSI_R;
        case 's': return kVK_ANSI_S;
        case 't': return kVK_ANSI_T;
        case 'u': return kVK_ANSI_U;
        case 'v': return kVK_ANSI_V;
        case 'w': return kVK_ANSI_W;
        case 'x': return kVK_ANSI_X;
        case 'y': return kVK_ANSI_Y;
        case 'z': return kVK_ANSI_Z;
        case '0': return kVK_ANSI_0;
        case '1': return kVK_ANSI_1;
        case '2': return kVK_ANSI_2;
        case '3': return kVK_ANSI_3;
        case '4': return kVK_ANSI_4;
        case '5': return kVK_ANSI_5;
        case '6': return kVK_ANSI_6;
        case '7': return kVK_ANSI_7;
        case '8': return kVK_ANSI_8;
        case '9': return kVK_ANSI_9;
        case '=': return kVK_ANSI_Equal;
        case '-': return kVK_ANSI_Minus;
        case ']': return kVK_ANSI_RightBracket;
        case '[': return kVK_ANSI_LeftBracket;
        case '\'': return kVK_ANSI_Quote;
        case ';': return kVK_ANSI_Semicolon;
        case '\\': return kVK_ANSI_Backslash;
        case ',': return kVK_ANSI_Comma;
        case '/': return kVK_ANSI_Slash;
        case '.': return kVK_ANSI_Period;
        case '`': return kVK_ANSI_Grave;
        case '\n': return kVK_Return;
        case '\t': return kVK_Tab;
        case ' ': return kVK_Space;
    }

    fprintf(stderr, "warning: unknown keycode for key '%c', using placeholder '-' instead\n", key);
    return kVK_ANSI_Minus;
}
#endif

/*
 * Injects input for the given key and whether it is down (vs. up).
 */
static void inject_input(const char key, const bool down)
{
    // printf("%c %d\n", key, down);
#ifdef __APPLE__
    CGKeyCode keycode = darwin_get_keycode(key);
    CGEventRef evt = CGEventCreateKeyboardEvent(NULL, keycode, down);
    CGEventPost(kCGSessionEventTap, evt);
    CFRelease(evt);
#endif
}

/*
 * Prints the command-line usage/help message to stderr.
 */
static void print_usage(void)
{
    fprintf(stderr, "usage: %s OPTION...\n", APPNAME);
    fprintf(stderr, "  -h, --help           print help message\n");
    fprintf(stderr, "  -l, --list           list devices\n");
    fprintf(stderr, "  -r, --run VID:PID    run with given device\n");
    fprintf(stderr, "  -w, --watch VID:PID  watch given device\n");
}

/*
 * Parses command-line args.
 * Returns 0 on success or non-zero if should exit early.
 */
static int parse_args(const int argc, const char * const *argv,
                      my_mode_t *mode, const char **dev_slug)
{
    switch (argc) {
        default:
            print_usage();
            return 1;

        case 1:
            *mode = MODE_RUN;
            *dev_slug = DEFAULT_DEVICE;
            break;

        case 2:
            if ((strcmp(argv[1], "-w") == 0) ||
                (strcmp(argv[1], "--watch") == 0)) {

                *mode = MODE_WATCH;
                *dev_slug = DEFAULT_DEVICE;
            } else if ((strcmp(argv[1], "-l") == 0) ||
                       (strcmp(argv[1], "--list") == 0)) {
                *mode = MODE_LIST;
            } else if ((strcmp(argv[1], "-h") == 0) ||
                       (strcmp(argv[1], "--help") == 0)) {
                print_usage();
                return 1;
            } else {
                *mode = MODE_RUN;
                *dev_slug = argv[1];
            }
            break;

        case 3:
            if ((strcmp(argv[1], "-w") == 0) ||
                (strcmp(argv[1], "--watch") == 0)) {

                *mode = MODE_WATCH;
                *dev_slug = argv[2];
            } else if ((strcmp(argv[1], "-r") == 0) ||
                       (strcmp(argv[1], "--run") == 0)) {
                *mode = MODE_RUN;
                *dev_slug = argv[2];
            } else {
                print_usage();
                return 1;
            }
            break;
    }

    return 0;
}

/*
 * Singal handler installed by set_signal_handlers.
 * Returns 0 on success.
 */
static void signal_handler(const int sig)
{
    fprintf(stderr, "...interrupted\n");
    exit_signal = sig;
}

/*
 * Sets signal_handler to be the handler for SIGINT and SIGTERM.
 * Returns 0 on success.
 */
static int set_signal_handlers(const bool custom)
{
    static int old_saved;
    static struct sigaction old_sigint_action;
    static struct sigaction old_sigterm_action;

    int r;

    if (custom) {
        struct sigaction action;
        action.sa_handler = signal_handler;
        sigemptyset(&action.sa_mask);
        action.sa_flags = 0;

        r = sigaction(SIGINT, &action, &old_sigint_action);
        if (r != 0) {
            fprintf(stderr, "error: unexpected init error: sigaction(SIGINT): %d\n", r);
            return r;
        }

        r = sigaction(SIGTERM, &action, &old_sigterm_action);
        if (r != 0) {
            fprintf(stderr, "error: unexpected init error: sigaction(SIGTERM): %d\n", r);
            return r;
        }

        old_saved = 1;
    } else {
        if (!old_saved)
            return 1;

        r = sigaction(SIGINT, &old_sigint_action, NULL);
        if (r != 0)
            return r;

        r = sigaction(SIGTERM, &old_sigterm_action, NULL);
        if (r != 0)
            return r;
    }

    return 0;
}

/*
 * Main entrypoint.
 */
int main(const int argc, const char * const argv[])
{
    const char *dev_slug;
    const size_t buflen = DEFAULT_BUFLEN;
    const int timeout_millis = DEFAULT_TIMEOUT_MILLIS;

    my_mode_t mode;
    hid_device *dev = NULL;

    if (parse_args(argc, argv, &mode, &dev_slug) != 0)
        return 1;

    if (set_signal_handlers(true) != 0)  // set custom signal handlers
        return 3;

    setbuf(stdout, NULL);  // turn off buffering on stdout

    if (hid_init() != 0) {
        fprintf(stderr, "error: unexpected init error: hid_init()\n");
        return 3;
    }
#ifdef __APPLE__
    hid_darwin_set_open_exclusive(0);
#endif

    if (mode != MODE_LIST) {
        uint16_t vid, pid;

        if (sscanf(dev_slug, "%4hx:%4hx", &vid, &pid) != 2) {
            fprintf(stderr, "error: malformed device VID:PID given: %s\n", dev_slug);
            return 1;
        }

        fprintf(stderr, "opening device %04X:%04X\n", vid, pid);
        dev = hid_open(vid, pid, NULL);
        if (dev == NULL) {
            fprintf(stderr, "error: could not open device %04X:%04X\n", vid, pid);
            return 2;
        }
    }

    switch (mode) {
        case MODE_LIST: {
            struct hid_device_info *infos, *cur;
            infos = hid_enumerate(0, 0);
            for (cur = infos; cur != NULL; cur = cur->next) {
                if (exit_signal)
                    break;

                printf("%04X:%04X - %ls %ls\n",
                       cur->vendor_id, cur->product_id,
                       cur->manufacturer_string, cur->product_string);
            }
            hid_free_enumeration(infos);
            break;
        }

        case MODE_RUN: {
            fprintf(stderr, "running...\n");

#ifdef __APPLE__
            if (!AXIsProcessTrusted()) {
                fprintf(stderr, "warning: process is not trusted, please add %s to Accessibility allow list!\n", APPNAME);
            }
#endif

            int r;
            while ((r = hid_read_timeout(dev, buf, buflen, timeout_millis)) >= 0) {
                if (exit_signal)
                    break;

                if (r == 0)
                    continue;

                handle_input(buf, r);
            }
            break;
        }

        case MODE_WATCH: {
            fprintf(stderr, "watching device for input...\n");

            int r;
            while ((r = hid_read_timeout(dev, buf, buflen, timeout_millis)) >= 0) {
                if (exit_signal)
                    break;

                if (r == 0)
                    continue;

                int newline_needed = 1;

                for (int i = 0; i < r; i++) {
                    printf(" %02x", buf[i]);
                    newline_needed = 1;
                    if ((i != 0) && ((i % 16) == 0)) {
                        newline_needed = 0;
                        printf("\n");
                    }
                }

                if (newline_needed)
                    printf("\n");

                memset(buf, 0, buflen);
            }
            break;
        }
    }

    if (dev != NULL) {
        fprintf(stderr, "closing device\n");
        hid_close(dev);
    }
    hid_exit();

    if (exit_signal) {
        if (set_signal_handlers(false) != 0)  // restore old signal handlers
            return exit_signal;
        kill(getpid(), exit_signal);
    }

    return 0;
}
