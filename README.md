# hidhornet

**hidhornet** is a small input injection utility for macOS that translates unrecognized gamepad / controller inputs into keyboard keypresses.

It works by reading raw controller inputs directly using [HIDAPI](https://github.com/libusb/hidapi) then injecting keypresses using [CGEventPost](https://developer.apple.com/documentation/coregraphics/1456527-cgeventpost).

Currently, **hidhornet** does not support re-configuring itself other than via re-compilation. See **Configuration** below for details.

## Requirements

* macOS
    * Any "recent" version of macOS should work, including macOS on Apple Silicon (e.g. M1 or M2).
    * However, **hidhornet** has only been tested on an M2 machine running macOS Ventura.
* Xcode Command Line Tools
    * Install with `xcode-select --install` from the Terminal if you do not already have Xcode or Xcode Command Line Tools.
* [HIDAPI](https://github.com/libusb/hidapi) library
    * The `Makefile` provided in this repo will automatically download HIDAPI into the subdirectory `hidapi/` during compilation.
    * Alternatively, you may provide a of HIDAPI yourself by placing it in the subdirectory `hidapi/`.

## Usage

### Compilation

```
git clone https://gitlab.com/icasdri/hidhornet
cd hidhornet
make
```

### Running

Depending on your macOS version, you may need to first add `hidhornet` and/or *Terminal* to the *Accessibility* and/or *Input Monitoring* allow list under *System Settings* -> *Privacy & Security*.

```
$ ./hidhonet --help
usage: hidhornet OPTION...
  -h, --help           print help message
  -l, --list           list devices
  -r, --run VID:PID    run with given device
  -w, --watch VID:PID  watch given device
```

### Configuration

`hidhornet.c` contains a section near the top titled `CONFIGURATION`. This currently includes an input injection routine `handle_input` written for an Xbox One controller (running version 1.x controller firmware) that does the following:

* Maps the left trigger to `s` on the keyboard
* Maps the right trigger to `c` on the keyboard
* Maps the share/select button to `i` on the keyboard

The above mapping was motivated by [Hollow Knight](https://www.hollowknight.com/)'s *Native Controller Support* recognizing almost all buttons/sticks but not the triggers or the share/select button.

Unless you happen to have the same controller (and controller firmware version) and want to play Hollow Knight, you will need to modify the `handle_input` routine to accomodate your controller and the game you want to play.

High-level steps to modify the `handle_input` routine:

1. Connect your controller
2. Run `./hidhornet --list` to list all input devices.
    - Then, identify the `VID:PID` identifier for your controller.
3. Run `./hidhornet --watch VID:PID` to watch for all input from your controller.
    - Then, poke any button/stick/trigger/etc. on your controller. You should see bytes being printed out.
    - Then, poke whichever button/stick/trigger/etc. that is not recognized by your game and try to figure out how that input is reflected in the bytes being printed out.
4. Modify the `handle_input` routine in `hidhornet.c` to read out whatever you found out above.
    - Note: `handle_input` receives an array of bytes `data` (containing `nread` number of bytes) for every "event" that your controller emits.
    - Tip: you can add `printf` statements in `handle_input` to help you debug along the way
    - Then, add calls to `inject_input` from `handle_input` to inject whichever key presses would be appropriate for your game.
5. Optionally change `DEFAULT_DEVICE` to be the `VID:PID` of your controller
6. Recompile **hidhornet** with `make` (see **Compilation** above)
7. Run `./hidhornet --run VID:PID` (or just `./hidhornet` if you set `DEFAULT_DEVICE` correctly)
8. Enjoy your game :)
    - Unless the game you are trying to play is [*Hollow Knight: Silk Song*](https://store.steampowered.com/app/1030300/hollow_knight_silksong), in which case *patiently* wait for Team Cherry to release it first.

## License and Acknowledgements

License: GPLv3+, see the comment at the top of `hidhornet.c` for details.

Acknowledgements:
- [hidapitester](https://github.com/todbot/hidapitester) for a good reference on how to use HIDAPI
- [pynput](https://github.com/moses-palmer/pynput) for a good reference on how to use macOS input injection APIs (e.g. `CGEventPost`)

